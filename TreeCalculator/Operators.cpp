#include "stdafx.h"
#include "Operators.h"
#include <valarray>

int AddOpr::apply(std::vector<int> operands)
{
	return operands[0] + operands[1];
}

int SubtractOpr::apply(std::vector<int> operands)
{
	return operands[0] - operands[1];
}

int MultiplyOpr::apply(std::vector<int> operands)
{
	return operands[0] * operands[1];
}

int DivideOpr::apply(std::vector<int> operands)
{
	return operands[0] / operands[1];
}

int SinusOpr::apply(std::vector<int> operands)
{
	return std::sin(operands[0]);
}

int CosinusOpr::apply(std::vector<int> operands)
{
	return std::cos(operands[0]);
}

int SuperAddOpr::apply(std::vector<int> operands)
{
	return operands[0] + operands[1] + operands[2];
}

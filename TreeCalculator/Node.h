﻿#pragma once
#include "IOperator.h"
#include <string>
#include <sstream>
#include <map>

class Node
{
public:
	Node();
	Node(Node &cOther);
	Node(std::vector<std::string>* symbols);
	void getFormula(std::ostringstream* out_formula);
	void getVariables(std::vector<std::string>** out_vars);
	Node* getLastLeaf();
	void setLastLeaf(Node* node);
	int compute(std::map<std::string, int> variable_vals);
	int getChildrenLen() { return _children.size(); }
	~Node();

private:
	std::string _symbol;
	bool _is_operator;
	bool _is_operand;
	bool _is_variable;
	IOperator* _operator;
	std::string _variable_name;
	int _constant;
	std::vector<Node*> _children;

};

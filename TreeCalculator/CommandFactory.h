﻿#pragma once
#include "ICommand.h"
#include <vector>

class CommandFactory
{
public:
	static ICommand* getCommand(std::vector<std::string> input, Result** result);
};

namespace CommandNames
{
	const std::string ENTER = "enter";
	const std::string PRINT_VARIABLES = "vars";
	const std::string PRINT = "print";
	const std::string COMPUTE = "comp";
	const std::string JOIN = "join";
}

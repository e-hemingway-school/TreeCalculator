﻿#pragma once
#include <vector>

class IOperator
{
public:
	IOperator() { }
	virtual int apply(std::vector<int> operands) = 0;
	virtual int getNumberOfOperands() = 0;
	virtual ~IOperator() { }
};

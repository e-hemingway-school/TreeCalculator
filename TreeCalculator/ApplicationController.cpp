﻿#include "stdafx.h"
#include "ApplicationController.h"
#include "ResultCode.h"
#include "Result.h"

ApplicationController::ApplicationController()
{
	_model = new ModelController();
	_view = new ViewController();
}

void ApplicationController::run()
{
	while (true)
	{
		ICommand* command = _view->getCommand();
		if(command != nullptr)
		{
			Result* command_result = nullptr;
			command->execute(&_model, &command_result);
			if (command_result->getResultCode() == ResultCode::SUCCESS && command_result->getMessage() != "")
			{
				_view->printInfoMessage(command_result->getMessage());
			}
			else if (command_result->getMessage() != "")
			{
				_view->printErrorMessage(command_result->getMessage());
			}

		}
	}
}

ApplicationController::~ApplicationController()
{
	delete _model;
	delete _view;
}

#pragma once
#include "IOperator.h"

class AddOpr: public IOperator
{
public:
	int apply(std::vector<int> operands) override;
	int getNumberOfOperands() override { return 2; }
	~AddOpr() override { }
};

class SubtractOpr: public IOperator
{
public:
	int apply(std::vector<int> operands) override;
	int getNumberOfOperands() override { return 2; }
	~SubtractOpr() override { }
};

class MultiplyOpr: public IOperator
{
public:
	int apply(std::vector<int> operands) override;
	int getNumberOfOperands() override { return 2; }
	~MultiplyOpr() override { }
};

class DivideOpr: public IOperator
{
public:
	int apply(std::vector<int> operands) override;
	int getNumberOfOperands() override { return 2; }
	~DivideOpr() override { }
};

class SinusOpr: public IOperator
{
public:
	int apply(std::vector<int> operands) override;
	int getNumberOfOperands() override { return 1; }
	~SinusOpr() override { }
};

class CosinusOpr: public IOperator
{
public:
	int apply(std::vector<int> operands) override;
	int getNumberOfOperands() override { return 1; }
	~CosinusOpr() override { }
};

class SuperAddOpr: public IOperator
{
public:
	int apply(std::vector<int> operands) override;
	int getNumberOfOperands() override { return 3; }
};

﻿#include "stdafx.h"
#include "ModelController.h"

ModelController::ModelController()
{
	_tree = new FormulaTree();
}

void ModelController::setTree(FormulaTree* new_tree)
{
	if (_tree != nullptr)
	{
		delete _tree;
	}
	_tree = new_tree;
}

FormulaTree* ModelController::getTree() const
{
	return _tree;
}

ModelController::~ModelController()
{
	if(_tree != NULL)
	{
		delete _tree;
	}
}

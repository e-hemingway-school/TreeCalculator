﻿#include "stdafx.h"
#include "Commands.h"
#include <stack>
#include "OperatorFactory.h"

static const std::string NOT_ENOUGH_OPERS = "Not enough operators";
static const std::string FORMULA_MSG = "Formula saved: ";
static const std::string WHITESPACE = " ";
static const std::string WRONG_NO_VARS = "Wrong number of variables";
static const char COMMAND_DELIMITER = ' ';

void EnterCmd::execute(ModelController** model_controller, Result** result)
{
	auto root_node = new Node(&_symbols);
	(*model_controller)->getTree()->setRootNode(root_node);
	auto formula = (*model_controller)->getTree()->toString();
	*result = new Result(ResultCode::SUCCESS, FORMULA_MSG + formula);
}

void JoinCmd::execute(ModelController** model_controller, Result** result)
{
//	auto formula = (*model_controller)->getTree()->toString();
//	std::vector<std::string> tokens;
//	std::istringstream stream(formula);
//	for (std::string token; std::getline(stream, token, COMMAND_DELIMITER);)
//	{
//		tokens.push_back(token);
//	}
//	tokens.pop_back();
//	tokens.reserve(tokens.size() + _symbols.size());
//	tokens.insert(tokens.end(), _symbols.begin(), _symbols.end());
//	auto root_node = new Node(&tokens);
//	(*model_controller)->getTree()->setRootNode(root_node);
	auto tree1 = (*model_controller)->getTree();
	auto tree2 = new FormulaTree();
	tree2->setRootNode(new Node(&_symbols));
	auto new_tree = *tree1 + *tree2;
	(*model_controller)->setTree(new_tree);
	*result = new Result(ResultCode::SUCCESS);
}

void PrintCmd::execute(ModelController** model_controller, Result** result)
{
	auto formula = (*model_controller)->getTree()->toString();
	*result = new Result(ResultCode::SUCCESS, formula);
}

void PrintVarsCmd::execute(ModelController** model_controller, Result** result)
{
	auto vars = (*model_controller)->getTree()->getVariables();
	std::ostringstream vars_stream;
	for (int i = 0; i < vars->size(); i++)
	{
		vars_stream << (*vars)[i] << WHITESPACE;
	}
	*result = new Result(ResultCode::SUCCESS, vars_stream.str());
}

void ComputeCmd::execute(ModelController** model_controller, Result** result)
{
	auto variables = (*model_controller)->getTree()->getVariables();
	if(_values.size() == variables->size())
	{
		std::map<std::string, int> values_map;
		for(int i = 0; i < variables->size(); i++)
		{
			values_map.insert_or_assign((*variables)[i], _values[i]);
		}
		auto formula_result = (*model_controller)->getTree()->compute(values_map);
		*result = new Result(ResultCode::SUCCESS, std::to_string(formula_result));
		delete variables;
	}
	else
	{
		*result = new Result(ResultCode::FAILURE, WRONG_NO_VARS);
	}
}

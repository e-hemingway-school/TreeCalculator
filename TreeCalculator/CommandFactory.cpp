﻿#include "stdafx.h"
#include "CommandFactory.h"
#include "ResultCode.h"
#include "Result.h"
#include "Commands.h"

static const std::string EMPTY_INPUT_MSG = "Input cannot be empty";
static const std::string NOT_ENOUGH_ARGS = "Not enough arguments";
static const std::string ARG_NOT_INT = "Arguments must be integers";
static const std::string UNKNOWN_CMD = "Unknown command";

ICommand* CommandFactory::getCommand(std::vector<std::string> input, Result** result)
{
	if(input.size() == 0)
	{
		*result = new Result(ResultCode::FAILURE, EMPTY_INPUT_MSG);
		return nullptr;
	}
	auto command_name = input[0];

	if(command_name == CommandNames::ENTER)
	{
		if (input.size() < EnterCmd::MIN_ARGS)
		{
			*result = new Result(ResultCode::FAILURE, NOT_ENOUGH_ARGS);
			return nullptr;
		}
		input.erase(input.begin());
		*result = new Result(ResultCode::SUCCESS);
		return new EnterCmd(input);
	}

	if(command_name == CommandNames::PRINT_VARIABLES)
	{
		*result = new Result(ResultCode::SUCCESS);
		return new PrintVarsCmd();
	}

	if(command_name == CommandNames::PRINT)
	{
		*result = new Result(ResultCode::SUCCESS);
		return new PrintCmd();
	}

	if(command_name == CommandNames::COMPUTE)
	{
		// TODO check empty
		std::vector<int> values;
		try
		{
			for(int i = 1; i < input.size(); i++)
			{
				int value = std::stoi(input[i]);
				values.push_back(value);
			}
			*result = new Result(ResultCode::SUCCESS);
			return new ComputeCmd(values);
		}
		catch (...)
		{
			*result = new Result(ResultCode::ILLEGAL_ARGUMENT, ARG_NOT_INT);
			return nullptr;
		}
	}

	if(command_name == CommandNames::JOIN)
	{
		if (input.size() < JoinCmd::MIN_ARGS)
		{
			*result = new Result(ResultCode::FAILURE, NOT_ENOUGH_ARGS);
			return nullptr;
		}

		input.erase(input.begin());
		*result = new Result(ResultCode::SUCCESS);
		return new JoinCmd(input);
	}

	*result = new Result(ResultCode::ILLEGAL_ARGUMENT, UNKNOWN_CMD);
	return nullptr;
}

﻿#include "stdafx.h"
#include "Result.h"

static const std::string EMPTY_MESSAGE = "";

Result::Result(ResultCode result_code):
	_result_code(result_code),
	_message(EMPTY_MESSAGE)
	{ }

Result::Result(ResultCode result_code, std::string message):
	_result_code(result_code),
	_message(message)
	{ }

ResultCode Result::getResultCode() const
{
	return _result_code;
}

void Result::setResultCode(ResultCode result_code)
{
	_result_code = result_code;
}

std::string Result::getMessage() const
{
	return _message;
}

void Result::setMessage(std::string message)
{
	_message = message;
}


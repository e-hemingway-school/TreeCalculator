﻿#pragma once
#include "ModelController.h"
#include "ViewController.h"

class ApplicationController
{
public:
	ApplicationController();
	void run();
	~ApplicationController();

private:
	ModelController* _model;
	ViewController* _view;
};

﻿#include "stdafx.h"
#include "Node.h"
#include "Result.h"
#include "OperatorFactory.h"
#include <map>

static const std::string DEFAULT_SYMBOL = "1";
static const std::string WHITESPACE = " ";


Node::Node()
{
	_operator = nullptr;
}

Node::Node(Node& cOther)
{
	_is_operator = cOther._is_operator;
	_is_operand = cOther._is_operand;
	_is_variable = cOther._is_variable;
	_symbol = cOther._symbol;
	_constant = cOther._constant;
	_variable_name = cOther._variable_name;

	if(cOther._operator != nullptr)
	{
		_operator = OperatorFactory::getOperator(cOther._symbol);
	}
	for(int i = 0; i < cOther._children.size(); i++)
	{
		auto child = new Node(*cOther._children[i]);
		_children.push_back(child);
	}
}

Node::Node(std::vector<std::string>* symbols)
{
	std::string current_symbol;
	if (symbols->size() == 0)
	{
		current_symbol = DEFAULT_SYMBOL;
	}
	else
	{
		current_symbol = (*symbols)[0];
		symbols->erase(symbols->begin());
	}
	_symbol = current_symbol;
	try
	{
		auto constant = std::stoi(current_symbol);
		_constant = constant;
		_operator = nullptr;
		_is_operator = false;
		_is_operand = true;
		_is_variable = false;
	}
	catch (...)
	{
		_operator = OperatorFactory::getOperator(current_symbol);
		if (_operator == nullptr)
		{
			_variable_name = current_symbol;
			_is_operator = false;
			_is_operand = true;
			_is_variable = true;
		}
		else
		{
			auto number_of_operands = _operator->getNumberOfOperands();
			_children.reserve(number_of_operands);
			for (int i = 0; i < number_of_operands; i++)
			{
				auto child = new Node(symbols);
				_children.push_back(child);
			}
			_is_operator = true;
			_is_operand = false;
			_is_variable = false;
		}
	}
}
void Node::getFormula(std::ostringstream* out_formula)
{
	(*out_formula) << _symbol << WHITESPACE;
	for (int i = 0; i < _children.size(); i++)
	{
		_children[i]->getFormula(out_formula);
	}
}

void Node::getVariables(std::vector<std::string>** out_vars)
{
	if(_is_variable)
	{
		(*out_vars)->push_back(_variable_name);
	}
	for(int i = 0; i < _children.size(); i++)
	{
		_children[i]->getVariables(out_vars);
	}
}

Node* Node::getLastLeaf()
{
	if(_children.size() == 0)
	{
		return this;
	}
	return _children[_children.size() - 1]->getLastLeaf();
}

void Node::setLastLeaf(Node* node)
{
	if(_children[_children.size() - 1]->_children.size() == 0)
	{
		delete _children[_children.size() - 1];
		_children.pop_back();
		_children.push_back(node);
	}
	else
	{
		_children[_children.size() - 1]->setLastLeaf(node);
	}
}

int Node::compute(std::map<std::string, int> variable_vals)
{
	if(_is_operand)
	{
		if(_is_variable)
		{
			return variable_vals[_variable_name];
		}
		return _constant;
	}
	std::vector<int> operands;
	for(int i = 0; i < _children.size(); i++)
	{
		operands.push_back(_children[i]->compute(variable_vals));
	}
	return _operator->apply(operands);
}

Node::~Node()
{
	if(_operator != NULL)
	{
		delete _operator;
	}
}


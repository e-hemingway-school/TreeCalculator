﻿#pragma once
#include "ResultCode.h"
#include <string>

class Result
{
public:
	Result(ResultCode result_code);
	Result(ResultCode result_code, std::string	message);
	ResultCode getResultCode() const;
	void setResultCode(ResultCode result_code);
	std::string getMessage() const;
	void setMessage(std::string message);

private:
	ResultCode _result_code;
	std::string _message;
};

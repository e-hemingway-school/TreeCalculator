// TreeCalculator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ApplicationController.h"


int main()
{
	auto app = new ApplicationController();
	app->run();
	delete app;
    return 0;
}


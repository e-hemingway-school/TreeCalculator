﻿#pragma once
#include "Node.h"

class FormulaTree
{
public:
	FormulaTree();
	FormulaTree(FormulaTree& other);
	void setRootNode(Node* root_node);
	std::string toString();
	std::vector<std::string>* getVariables();
	int compute(std::map<std::string, int> variable_vals);
	void operator = (FormulaTree other);
	FormulaTree* operator + (FormulaTree other);
	~FormulaTree();

private:
	Node* _root_node;

};

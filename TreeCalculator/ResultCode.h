#pragma once
enum class ResultCode
{
	SUCCESS,
	FAILURE,
	ILLEGAL_ARGUMENT
};
﻿#include "stdafx.h"
#include "OperatorFactory.h"
#include "Operators.h"
#include "Result.h"

static const std::string UNKNOWN_OPR = "Unknown operator";

IOperator* OperatorFactory::getOperator(std::string symbol)
{
	if(symbol == OperatorSymbols::ADD)
	{
		return new AddOpr();
	}

	if(symbol == OperatorSymbols::SUB)
	{
		return new SubtractOpr();
	}

	if(symbol == OperatorSymbols::DIV)
	{
		return new DivideOpr();
	}
	
	if(symbol == OperatorSymbols::MULT)
	{
		return new MultiplyOpr();
	}
	
	if(symbol == OperatorSymbols::SIN)
	{
		return new SinusOpr();
	}

	if(symbol == OperatorSymbols::COS)
	{
		return new CosinusOpr();
	}

	if(symbol == OperatorSymbols::SUP_ADD)
	{
		return new SuperAddOpr();
	}

	return nullptr;
}

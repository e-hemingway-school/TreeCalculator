﻿#include "stdafx.h"
#include "FormulaTree.h"
#include <sstream>

static const std::string EMPTY_STRING = "";

FormulaTree::FormulaTree()
{
	_root_node = new Node();
}

FormulaTree::FormulaTree(FormulaTree& other)
{
	_root_node = new Node(*other._root_node);
}

void FormulaTree::setRootNode(Node* root_node)
{
	if(_root_node != nullptr)
	{
		delete _root_node;
	}
	_root_node = root_node;
}

std::string FormulaTree::toString()
{
	if(_root_node == nullptr)
	{
		return EMPTY_STRING;
	}
	auto formula = new std::ostringstream();
	_root_node->getFormula(formula);
	auto formula_str = formula->str();
	delete formula;
	return formula_str;
}

std::vector<std::string>* FormulaTree::getVariables()
{
	std::vector<std::string>* vars = new std::vector<std::string>();
	if(_root_node == nullptr)
	{
		return vars;
	}
	_root_node->getVariables(&vars);
	return vars;
}

int FormulaTree::compute(std::map<std::string, int> variable_vals)
{
	return _root_node->compute(variable_vals);
}

void FormulaTree::operator=(FormulaTree other)
{
	if(_root_node != nullptr)
	{
		delete _root_node;
	}
	_root_node = new Node(*other._root_node);
}

FormulaTree* FormulaTree::operator+(FormulaTree other)
{
	auto node_to_push = new Node(*other._root_node);
	auto new_tree = new FormulaTree(*this);
	if(new_tree->_root_node->getChildrenLen() == 0)
	{
		new_tree->setRootNode(node_to_push);
	}
	else
	{
		new_tree->_root_node->setLastLeaf(node_to_push);
	}
	return new_tree;
}

FormulaTree::~FormulaTree()
{
	if (_root_node != nullptr)
	{
		delete _root_node;
	}
}

﻿#pragma once
#include "ICommand.h"
#include <vector>
#include "IOperator.h"
#include <string>

class EnterCmd: public ICommand
{
public:
	EnterCmd(std::vector<std::string> symbols) : _symbols(symbols) { }
	void execute(ModelController** model_controller, Result** result) override;

	static const int MIN_ARGS = 2;

private:
	std::vector<std::string> _symbols;

};

class JoinCmd: public ICommand
{
public:
	JoinCmd(std::vector<std::string> symbols): _symbols(symbols) { }
	void execute(ModelController** model_controller, Result** result) override;

	static const int MIN_ARGS = 2;
private:
	std::vector <std::string> _symbols;
};

class PrintCmd: public ICommand
{
public:
	void execute(ModelController** model_controller, Result** result) override;

};

class PrintVarsCmd: public ICommand
{
public:
	void execute(ModelController** model_controller, Result** result) override;

};

class ComputeCmd: public ICommand
{
public:
	ComputeCmd(std::vector<int> values) : _values(values) { }
	void execute(ModelController** model_controller, Result** result) override;

private:
	std::vector<int> _values;
};

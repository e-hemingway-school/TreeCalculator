﻿#pragma once
#include "IOperator.h"
#include <string>
#include "Result.h"

class OperatorFactory
{
public:
	static IOperator* getOperator(std::string symbol);
};

namespace OperatorSymbols
{
	const std::string ADD = "+";
	const std::string SUB = "-";
	const std::string MULT = "*";
	const std::string DIV = "/";
	const std::string SIN = "sin";
	const std::string COS = "cos";
	const std::string SUP_ADD = "++";
}

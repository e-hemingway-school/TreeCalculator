﻿#pragma once
#include "ModelController.h"

class Result;

class ICommand
{
public:
	ICommand() { }
	virtual void execute(ModelController** model_controller, Result** result) = 0;
	virtual ~ICommand() { }
};

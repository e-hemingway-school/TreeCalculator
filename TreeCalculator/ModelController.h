﻿#pragma once
#include "FormulaTree.h"

class ModelController
{
public:
	ModelController();
	void setTree(FormulaTree* new_tree); 
	FormulaTree* getTree() const;
	~ModelController();

private:
	FormulaTree* _tree;
};
